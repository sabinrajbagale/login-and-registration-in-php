<!DOCTYPE html>
<html>
	<head>
		<title>Login and Signup Form </title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="copl-md-12 col-xs-12 col-sm-12">
					<form action="registration.php" method="post" >
						<div class="login" align="right">
							<img  src="images/log.png" height="32px" width="50px" align="left">
							<label>Email</label>
							<input type="email" name="email" required  placeholder="enter your email." />
							<label>Password</label>
							<input type="password" name="password" required  class="password" placeholder="enter your password." />
							<input type="submit"  name="login" value="LogIn" class="agileinfo" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-xs-12">
					<h2 align="center">It helps you connect & share with the people.</h2>
					<img  src="images/fb.png" height="340px" width="500px" align="center"/><br>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.
				</div>
				<div class="col-md-4 col-xs-12">
					<div class="registration">
						<form action="registration.php" method="post" enctype="multipart/form-data" class="agile_form">
							<h1>Create an account</h1>
							<label>Username</label>
							<input type="text" name="username" class="form-control" required  placeholder="Enter your username." />
							<label>Email</label>
							<input type="email" name="email"class="form-control"  required  placeholder="Enter your email." />
							<label>Password</label>
							<input type="password" name="password1" id="password1"class="form-control"   required  placeholder="Enter your password.">
							<label>Confirm Password</label>
							<input type="password" name="password2" id="password2"  class="form-control" required  placeholder="Re-enter your password.">
							<label>Phone</label>
							<input type="text" name="phone" minlength="9" maxlength="10" class="form-control" required  placeholder="Enter your Phone No." />
							<label>Designation</label>
							<input type="text" name="designation" class="form-control" required  placeholder="Enter your Designation."/><br>
							
							<label>Profile Image</label><input type="file" name="img"  required  />
							<br>
							<label>Gender</label><select name="gender" required>
							<option>Male</option>
							<option>Female</option>
							<option>Others</option>
						</select>
						<label>Role</label><select name="role"  required>
						<option>Admin</option>
						<option>User</option>
					</select>
					<input type="submit" name="register" value="Sign Up">
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
			window.onload = function () {
				document.getElementById("password1").onchange = validatePassword;
				document.getElementById("password2").onchange = validatePassword;
			}
			function validatePassword(){
				var pass2=document.getElementById("password2").value;
				var pass1=document.getElementById("password1").value;
				if(pass1!=pass2)
					document.getElementById("password2").setCustomValidity("Passwords Don't Match");
				else
						document.getElementById("password2").setCustomValidity('');
					
			}
	</script>
</body>
</html>