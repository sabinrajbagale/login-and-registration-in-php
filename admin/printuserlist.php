<!DOCTYPE html>
<html>
	<head>
		<title>User List</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<body>
		<button onclick="mywholepage()">print</button>
		<script type="text/javascript">
			function mywholepage(){
				window.print();
			}
		</script>
		<div class="container">
				<table align="center">
					<tr>
						<th>SN</th>
						<th>Username</th>
						<th>Email</th>
						<th>Password</th>
						<th>Phone</th>
						<th>Gender</th>
						<th>Designation</th>
						<th>Role</th>

						
					</tr>
					
					<?php
					include('../dbcon.php');
					
					$qry="SELECT * FROM `admin` ";
					$run=mysqli_query($con,$qry);
					?>
					
					<?php
					$count=0;
					while($data=mysqli_fetch_array($run))
					{
					$count++;
					?>
					<tr align="center">
						<td><?php echo $count;   ?></td>
						<td><?php echo $data['username']; ?></td>
						<td><?php echo $data['email']; ?></td>
						<td><?php echo $data['password1']; ?></td>
						<td><?php echo $data['phone']; ?></td>
						<td><?php echo $data['gender']; ?></td>
						<td><?php echo $data['designation']; ?></td>
						<td><?php echo $data['role']; ?></td>
					</tr>
					<?php
				}
				?>


				<!-- these js files are used for making PDF -->
				<script src="../js/jspdf.js"></script>
				<script src="../js/jquery-2.1.3.js"></script>
				<script src="../js/pdfFromHTML.js"></script>
				</table>
				
		</div>
	</tbody>
</body>
</html>