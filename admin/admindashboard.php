<!DOCTYPE html>
<html>
	<head>
		<title>Admindashboard</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<style>
		h1{
		font-size: 40px;
		color: red;
		text-shadow: 5px 8px 12px green;
		text-align: center;
		top: 1%
		}
		.navbar{
			height: 100%;
			width: 100%;
		}
		.sidebar hr{
		height: 3px;
		background-image: linear-gradient(to right,#00ada7 0%, #00ada7 950%);
		border: 0;
		}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 col-xs-12 col-12 p-2" >
					<H1>  WELCOME TO ADMIN DASHBOARD.</H1>
				</div>
			</div>
		</div>
		
		
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 col-xs-2">
					<nav class="navbar bg-dark sidebar" >
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link"  href="#">Dashboard</a>
							</li><hr>
							<li class="nav-item">
								<a class="nav-link" id="profile" href="#">User</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="admindashboard.php">Edit Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="userlist" href="#">User List</a>
							</li>
							
							<li class="nav-item">
								<a class="nav-link" id="printuserlist" href="#">Print Userlist</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="chat" href="#">chat</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="logout.php">Logout</a>
							</li>
							
						</ul>
					</nav>
				</div>
				<div class="col-md-10 col-xs-10">
					<div class="container">
						<div id="records_content">
						</div>
					</div>
					<!-- //////////////// after update ////////////////// -->
					<div class="modal fade" id="update_user_modal">
						<div class="modal-dialog">
							<div class="modal-content">
								<!-- Modal Header -->
								<div class="modal-header">
									<h4 class="modal-title">Modal Heading</h4>
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<!-- Modal body -->
								<div class="modal-body">
									
									<div class="form-group">
										<label> Username </label>
										<input type="text" name="username" id="update_username" placeholder="Userame" class="form-control">
									</div>
									<div class="form-group">
										<label> Email </label>
										<input type="text" name="email" id="update_email" placeholder="Email Id" class="form-control">
									</div>
									<div class="form-group">
										<label> Phone </label>
										<input type="text" name="phone" id="update_phone" placeholder="phone No." class="form-control">
									</div>
									<div class="form-group">
										<label> Designation </label>
										<input type="text" name="designation" id="update_designation" placeholder="designation No." class="form-control">
									</div>
									<div class="form-group">
										<label> role </label>
										<input type="text" name="role" id="update_role" placeholder="role" class="form-control">
									</div>
								</div>
								<!-- Modal footer -->
								<div class="modal-footer">
									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" onclick="UpdateUserDetails()" >Update</button>
									<input type="hidden" id="hidden_user_id">
								</div>
							</div>
						</div>
					</div>
				</div>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
				<script>
				$(document).ready(function () {
					readRecords();
					$('#userlist').click(function(){
						$('#records_content').load('backend.php?readrecords=1',function(){
						
						});
					});
					$('#printuserlist').click(function(){
						$('#records_content').load('printuserlist.php?readrecords=1',function(){
						
						});
					});
					$('#profile').click(function(){
						$('#records_content').load('profile.php?readrecords=1',function(){
						
						});
					});
					$('#chat').click(function(){
						$('#records_content').load('../chat/index.php?readrecords=1',function(){
						
						});
					});
				});
				//////////////////Display Records
				function readRecords(){
				
				var readrecords = "readrecords";
				$.ajax({
				url:"default.php",
				type:"POST",
				data:{readrecords:readrecords},
				success:function(data,status){
				$('#records_content').html(data);
				},
				});
				}
				/////////////delete userdetails ////////////
				function DeleteUser(deleteid){
				var conf = confirm("are u sure");
				if(conf == true) {
				$.ajax({
				url:"backend.php",
				type:'POST',
				data: {  deleteid : deleteid},
				success:function(data, status){
				readRecords();
				}
				});
				}
				}
				function GetUserDetails(id){
				$("#hidden_user_id").val(id);
				$.post("backend.php", {
				id: id
				},
				function (data, status) {
				
				//JSON.parse() parses a string, written in JSON format, and returns a JavaScript object.
				var user = JSON.parse(data);
				alert(user);
				$('#update_username').val(user.username);
				$('#update_email').val(user.email);
				$('#update_phone').val(user.phone);
				$('#update_designation').val(user.designation);
				$('#update_gender').val(user.gender);
				$('#update_role').val(user.role);
				}
				);
				$('#update_user_modal').modal("show");
				}
				function UpdateUserDetails() {
					var username = $("#update_username").val();
					var email = $("#update_email").val();
					var phone = $("#update_phone").val();
					var designation= $("#update_designation").val();
					var gender = $("#update_gender").val();
					var role = $("#update_role").val();
					var hidden_user_id = $("#hidden_user_id").val();
					$.post("backend.php", {
					hidden_user_id: hidden_user_id,
					username: username,
					email: email,
					password: password,
					phone: phone,
					designation: designation,
					gender: gender,
					role: role
					},
					function (data, status) {
					$("#update_user_modal").modal("hide");
					readRecords();
					}
					);
				}
				</script>
			</body>
		</html>